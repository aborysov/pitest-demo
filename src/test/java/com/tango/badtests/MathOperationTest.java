package com.tango.badtests;

import com.tango.MathOperation;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MathOperationTest {
    private MathOperation mathOperation = new MathOperation();

    @Test
    public void testWithSomeValues() throws Exception {

        int result = mathOperation.calculate(10, 5);

        assertThat(result).isNotZero();
    }

    @Test
    public void testWithSomeOtherÅValues() throws Exception {

        int result = mathOperation.calculate(5, 10);

        assertThat(result).isNotZero();
    }


}