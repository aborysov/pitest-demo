package com.tango.badtests;

import com.tango.Endpoint;
import com.tango.MessageSender;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Clock;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;

/**
 * I don't like write tests, but my boss made me do it!
 */
@RunWith(MockitoJUnitRunner.class)
public class MessageSenderTest {

    @Mock
    private Endpoint endpoint;
    private MessageSender sender;

    private Long senderId = 100L;
    private Long receiverId = 200L;

    @Before
    public void createSender() throws Exception {
        sender = new MessageSender(endpoint, Clock.systemUTC());
    }

    @Test
    public void myBossMadeMyTestIt() throws Exception {

        sender.send(senderId, receiverId, MessageSender.MessageFormat.XML);

        verify(endpoint).send(anyLong(), anyString());
    }

    @Test
    public void iNeedMoreCoverage() throws Exception {

        sender.send(senderId, receiverId, MessageSender.MessageFormat.JSON);

        verify(endpoint).send(anyLong(), anyString());
    }

    @Test
    public void ifThereIsAMethodIHaveToCoverIt() throws Exception {
        sender.setEnabled(false);

        sender.send(senderId, receiverId, MessageSender.MessageFormat.JSON);
    }

    @Test
    public void coverExceptionCase() throws Exception {
        try {
            sender.send(null, 2L, MessageSender.MessageFormat.JSON);
        } catch (Exception ex) {}
    }

    @Test
    public void coverAnotherException() throws Exception {
        try {
            sender.send(1L, null, MessageSender.MessageFormat.JSON);
        } catch (Exception ex) {}
    }

}
