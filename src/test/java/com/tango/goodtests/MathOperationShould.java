package com.tango.goodtests;

import com.tango.MathOperation;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for {@link com.tango.MathOperation} class.
 */
@RunWith(JUnitParamsRunner.class)
public class MathOperationShould {

    private MathOperation mathOperation = new MathOperation();

    @Test
    @Parameters({"1, 1, 2",
                "10000, 10001, 20001" ,
                "56, 150, 206",
                "0, 0, 0",
                "-100, 0, -100",
                "-987654, -80, -987734",
                "-10, 5, -5"})
    public void sumArgumentsIfFirstNotGreaterThanSecond(int first, int second, int expectedResult) {

        int result = mathOperation.calculate(first, second);

        assertThat(result).isEqualTo(expectedResult);
    }


    @Test
    @Parameters({"2, 1, 1",
                "10005, 10001, 4" ,
                "50, 0, 50",
                "0, -500, 500",
                "-80, -987654, 987574",
                "10, -50, 60"})
        public void subtractSecondFromFirstIfFirstGreaterThanSecond(int first, int second, int expectedResult) {

        int result = mathOperation.calculate(first, second);

        assertThat(result).isEqualTo(expectedResult);
    }

}
