package com.tango.goodtests;

import com.tango.Endpoint;
import com.tango.MessageSender;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.skyscreamer.jsonassert.JSONAssert;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import static org.custommonkey.xmlunit.XMLAssert.assertXpathEvaluatesTo;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Tests {@link com.tango.MessageSender} class.
 */
@RunWith(MockitoJUnitRunner.class)
public class MessageSenderShould {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private Endpoint endpoint;
    private MessageSender sender;

    @Captor
    private ArgumentCaptor<String> messageCaptor;

    private Long senderId = 100L;
    private Long receiverId = 200L;
    private Long epochMillis = 1430000000L;
    private Clock clock = Clock.fixed(Instant.ofEpochMilli(epochMillis), ZoneId.systemDefault());

    @Before
    public void createSender() throws Exception {
        sender = new MessageSender(endpoint, clock);
    }

    @Test
    public void sendXmlMessageIfFormatIsXml() throws Exception {

        sender.send(senderId, receiverId, MessageSender.MessageFormat.XML);

        verify(endpoint).send(anyLong(), messageCaptor.capture());
        String message = messageCaptor.getValue();
        assertXpathEvaluatesTo(senderId.toString(), "//Message/senderId[text()]", message);
        assertXpathEvaluatesTo(epochMillis.toString(), "//Message/timestamp[text()]", message);
    }

    @Test
    public void sendMessageToReceiver() throws Exception {

        sender.send(senderId, receiverId, MessageSender.MessageFormat.XML);

        verify(endpoint).send(eq(receiverId), anyString());
    }

    @Test
    public void sendJsonMessageIfFormatIsJson() throws Exception {

        sender.send(senderId, receiverId, MessageSender.MessageFormat.JSON);

        verify(endpoint).send(anyLong(), messageCaptor.capture());
        String message = messageCaptor.getValue();
        String expectedJson = "{senderId:" + senderId + ",timestamp:" + epochMillis + "}";
        JSONAssert.assertEquals(expectedJson, message, false);
    }

    @Test
    public void notSendMessageIfDisabled() throws Exception {
        sender.setEnabled(false);

        sender.send(senderId, receiverId, MessageSender.MessageFormat.JSON);

        verifyZeroInteractions(endpoint);
    }

    @Test
    public void throwIllegalArgumentExceptionIfSenderIdIsNull() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("null");
        expectedException.expectMessage(receiverId.toString());

        sender.send(null, receiverId, MessageSender.MessageFormat.JSON);
    }

    @Test
    public void coverAnotherException() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("null");
        expectedException.expectMessage(senderId.toString());

        sender.send(senderId, null, MessageSender.MessageFormat.JSON);
    }

}
