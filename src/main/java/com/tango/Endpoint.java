package com.tango;

public interface Endpoint {

    void send(Long receiverId, String message);

}
