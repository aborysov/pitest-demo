package com.tango;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.time.Clock;

public class MessageSender {

    public static enum MessageFormat {
        XML, JSON
    }

    private final ObjectMapper xmlMapper = new XmlMapper();
    private final ObjectMapper jsonMapper = new ObjectMapper();

    private final Endpoint endpoint;
    private final Clock clock;
    private boolean enabled = true;

    public MessageSender(Endpoint endpoint, Clock clock) {
        this.endpoint = endpoint;
        this.clock = clock;
    }

    public void send(Long senderId, Long receiverId, MessageFormat format) throws Exception {
        if (!enabled) {
            return;
        }
        if (senderId == null || receiverId == null) {
            String errorMessage = String.format(
                    "Sender if and receiver id must not be null. Sender id %s; receiver id %s", senderId, receiverId);
            throw new IllegalArgumentException(errorMessage);
        }

        String message = buildStringMessage(senderId, format);

        endpoint.send(receiverId, message);
    }

    private String buildStringMessage(Long senderId, MessageFormat format) throws Exception {
        ObjectMapper converter;
        if (format == MessageFormat.JSON) {
            converter = jsonMapper;
        } else {
            converter = xmlMapper;
        }

        Message message = new Message(senderId, clock.millis());
        return converter.writeValueAsString(message);
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    private static class Message {
        private final long senderId;
        private final long timestamp;

        private Message(long senderId, long timestamp) {
            this.senderId = senderId;
            this.timestamp = timestamp;
        }

        public long getSenderId() {
            return senderId;
        }

        public long getTimestamp() {
            return timestamp;
        }
    }

}
