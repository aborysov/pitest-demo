package com.tango;

public class MathOperation {

    public int calculate(int first, int second) {
        int result;
        if (first > second) {
            result = first - second;
        } else {
            result = second + first;
        }
        return result;
    }

}
